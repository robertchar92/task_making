Requirements:
1. Go v1.20.3
2. PostgreSQL
3. Install golang goose with command "go install github.com/pressly/goose/v3/cmd/goose@latest" for database migration
4. Install Makefile (optional)


How to run :
1. Run in command line "make dep". If not using Makefile run "go mod tidy" and "go mod vendor"
2. Create .env file using .env.example as template and fill requirements variable
3. Create PostgreSQL database
4. Run migration:
   - Makefile : Run "make migrate-up"
   - Command Line : Run goose -dir "{GOOSE_MIGRATION_DIR}" "{GOOSE_DRIVER}" "{GOOSE_DBSTRING}" up
    Replace {GOOSE_MIGRATION_DIR} with migration file path in this case is "db/migrations"
    Replace {GOOSE_DRIVER} with migration file path in this case is "postgres"
    Replace {GOOSE_DBSTRING} with migration file path in this case is "user="DATABASE_USERNAME" password="DATABASE_PASSWORD" dbname="DATABASE_NAME" sslmode=disable"


5. To run locally run "make dev" or ("go build -o bin/task_making app/main.go" and "./bin/task_making")
6. API Documentation in folder insomnia
