package request

import "task_making/utils/request_util"

type TaskCreateRequest struct {
	Name        string `form:"name" json:"name" binding:"required"`
	Description string `form:"description" json:"description" binding:"required"`
}

type TaskUpdateRequest struct {
	Name        *string `form:"name" json:"name" binding:"omitempty"`
	Description *string `form:"description" json:"description" binding:"omitempty"`
	Position    *int    `form:"position" json:"position" binding:"omitempty,min=0"`
	IsComplete  *bool   `form:"is_complete" json:"is_complete" binding:"omitempty"`
}

func NewTaskPaginationConfig(conditions map[string][]string) request_util.PaginationConfig {
	request_util.OverrideKey(conditions, "type", "scope")

	filterable := map[string]string{
		"id":          request_util.IdType,
		"name":        request_util.StringType,
		"is_complete": request_util.BoolType,
		"created_at":  request_util.DateType,
	}

	// conditions["sort"] = []string{"created_at DESC"}

	return request_util.NewRequestPaginationConfig(conditions, filterable)
}
