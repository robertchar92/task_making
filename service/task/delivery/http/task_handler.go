package http

import (
	"task_making/app/middleware"
	"task_making/service/task"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	taskUsecase task.Usecase
}

func New(taskUC task.Usecase) *Handler {
	return &Handler{
		taskUsecase: taskUC,
	}
}

func (h *Handler) Register(r *gin.Engine, m *middleware.Middleware) {
	userRoute := r.Group("/task", m.AuthHandle(), m.UserHandle())
	{
		userRoute.GET("", h.Index)
		userRoute.GET("/:id", h.Show)
		userRoute.POST("", h.Create)
		userRoute.PATCH("/:id", h.Update)
		userRoute.DELETE("/:id", h.Delete)
	}
}
