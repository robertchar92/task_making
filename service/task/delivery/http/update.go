package http

import (
	"fmt"
	"net/http"
	"strconv"
	"task_making/service/task/delivery/http/request"
	"task_making/utils/errors"
	"task_making/utils/role"

	"github.com/gin-gonic/gin"

	jwt "github.com/appleboy/gin-jwt/v2"
)

func (h *Handler) Update(c *gin.Context) {
	var req request.TaskUpdateRequest

	// validate request
	if err := c.ShouldBind(&req); err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypeBind)
		return
	}

	claims := jwt.ExtractClaims(c)

	userID, err := strconv.ParseUint(fmt.Sprint(claims["id"]), 10, 64)
	if err != nil {
		_ = c.Error(errors.ErrUnprocessableEntity).SetType(gin.ErrorTypePublic)
	}

	taskID, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		_ = c.Error(errors.ErrUnprocessableEntity).SetType(gin.ErrorTypePublic)
	}

	taskM, err := h.taskUsecase.Update(userID, taskID, req)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	data, err := role.GetDataJSONByRole(taskM, role.Admin)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	c.JSON(http.StatusOK, data)
}
