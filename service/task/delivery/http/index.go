package http

import (
	"fmt"
	"net/http"
	"strconv"
	"task_making/service/task/delivery/http/request"
	"task_making/utils/errors"
	response_util "task_making/utils/response_utils"
	"task_making/utils/role"

	jwt "github.com/appleboy/gin-jwt/v2"

	"github.com/gin-gonic/gin"
)

func (h *Handler) Index(c *gin.Context) {
	claims := jwt.ExtractClaims(c)

	userID, err := strconv.ParseUint(fmt.Sprint(claims["id"]), 10, 64)
	if err != nil {
		_ = c.Error(errors.ErrUnprocessableEntity).SetType(gin.ErrorTypePublic)
	}

	tasks, taskPagination, err := h.taskUsecase.Index(userID, request.NewTaskPaginationConfig(c.Request.URL.Query()))
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	data, err := role.GetDataJSONByRole(tasks, role.User)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	c.JSON(http.StatusOK, response_util.IndexResponse{
		Data: data,
		Meta: taskPagination,
	})
}
