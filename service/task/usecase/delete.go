package usecase

import "task_making/utils/errors"

func (u *Usecase) Delete(userID uint64, taskID uint64) error {
	taskM, err := u.taskRepo.FindByID(taskID)
	if err != nil {
		err := errors.ErrNotFound
		err.Message = "Task not found"
		return err
	}

	if taskM.UserID != userID {
		err := errors.ErrUnauthorized
		err.Message = "This task is not authorized for this user"
		return err
	}

	tx := u.transactionManager.NewTransaction()

	err = u.taskRepo.UpdatePositionWhenDelete(userID, taskM.Position, tx)
	if err != nil {
		tx.Rollback()
		err := errors.ErrUnprocessableEntity
		err.Message = "Failed to delete task."
		return err
	}

	err = u.taskRepo.Delete(taskM, tx)
	if err != nil {
		tx.Rollback()
		err := errors.ErrUnprocessableEntity
		err.Message = "Fail to delete task."
		return err
	}

	tx.Commit()

	return nil
}
