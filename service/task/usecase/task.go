package usecase

import (
	"task_making/lib/database_transaction"
	"task_making/service/task"
	"task_making/service/user"
)

type Usecase struct {
	userRepo           user.Repository
	taskRepo           task.Repository
	transactionManager database_transaction.Client
}

func New(
	userRepo user.Repository,
	taskRepo task.Repository,
	transactionManager database_transaction.Client,
) task.Usecase {
	return &Usecase{
		userRepo:           userRepo,
		taskRepo:           taskRepo,
		transactionManager: transactionManager,
	}
}
