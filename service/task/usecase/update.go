package usecase

import (
	"task_making/models"
	"task_making/service/task/delivery/http/request"
	"task_making/utils/errors"

	"github.com/jinzhu/copier"
)

func (u *Usecase) Update(userID uint64, taskID uint64, request request.TaskUpdateRequest) (*models.Task, error) {
	taskM, err := u.taskRepo.FindByID(taskID)
	if err != nil {
		err := errors.ErrNotFound
		err.Message = "Task not found"
		return nil, err
	}

	if taskM.UserID != userID {
		err := errors.ErrUnauthorized
		err.Message = "This task is not authorized for this user"
		return nil, err
	}

	tx := u.transactionManager.NewTransaction()

	if request.Position != nil {
		_, err = u.taskRepo.FindByPosition(userID, *request.Position)
		if err != nil {
			err := errors.ErrUnprocessableEntity
			err.Message = "Position not in range!"
			return nil, err
		}

		newPosition := *request.Position

		if newPosition < taskM.Position {
			err = u.taskRepo.UpdatePositionAddByOne(userID, newPosition, taskM.Position, tx)
			if err != nil {
				tx.Rollback()
				err := errors.ErrUnprocessableEntity
				err.Message = "Failed to update task position."
				return nil, err
			}
		} else if newPosition > taskM.Position {
			err = u.taskRepo.UpdatePositionSubByOne(userID, newPosition, taskM.Position, tx)
			if err != nil {
				tx.Rollback()
				err := errors.ErrUnprocessableEntity
				err.Message = "Failed to update task position."
				return nil, err
			}
		}

	}

	// copy content of request into model found by id
	_ = copier.Copy(taskM, &request)

	err = u.taskRepo.Update(taskM, tx)
	if err != nil {
		tx.Rollback()
		err := errors.ErrUnprocessableEntity
		err.Message = "Failed to update task."
		return nil, err
	}

	tx.Commit()

	return taskM, nil
}
