package usecase

import (
	"task_making/models"
	"task_making/service/task/delivery/http/request"
	"task_making/utils/errors"
)

func (u *Usecase) Create(userID uint64, request request.TaskCreateRequest) (*models.Task, error) {
	var taskM *models.Task

	tx := u.transactionManager.NewTransaction()

	taskM = &models.Task{
		Name:        request.Name,
		Description: request.Description,
		UserID:      userID,
	}

	err := u.taskRepo.UpdateAllPositionAddByOne(userID, tx)
	if err != nil {
		tx.Rollback()
		err := errors.ErrUnprocessableEntity
		err.Message = "Failed to create task."
		return nil, err
	}

	err = u.taskRepo.Insert(taskM, tx)
	if err != nil {
		tx.Rollback()
		err := errors.ErrUnprocessableEntity
		err.Message = "Failed to create task."
		return nil, err
	}

	tx.Commit()

	return taskM, nil
}
