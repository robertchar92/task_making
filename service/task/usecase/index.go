package usecase

import (
	"task_making/lib/scope"
	"task_making/models"
	"task_making/utils/errors"
	"task_making/utils/request_util"
	response_util "task_making/utils/response_utils"
)

func (u *Usecase) Index(userID uint64, paginationConfig request_util.PaginationConfig) ([]models.Task, response_util.PaginationMeta, error) {
	meta := response_util.PaginationMeta{
		Offset: paginationConfig.Offset(),
		Limit:  paginationConfig.Limit(),
		Total:  0,
	}

	paginationConfig.AddScope(scope.WhereIsScope("user_id", userID))

	// newPaginationConfig := request_util.NewPaginationConfig(paginationConfig.Limit(), paginationConfig.Offset(), "position ASC", paginationConfig.Scopes()...)

	tasks, err := u.taskRepo.FindAll(paginationConfig)
	if err != nil {
		err := errors.ErrNotFound
		err.Message = "Tasks not found"
		return nil, meta, err
	}

	total, err := u.taskRepo.Count(paginationConfig)
	if err != nil {
		err := errors.ErrNotFound
		err.Message = "Tasks not found"
		return nil, meta, err
	}

	meta.Total = total

	return tasks, meta, nil
}
