package usecase

import (
	"task_making/models"
	"task_making/utils/errors"
)

func (u *Usecase) Show(userID uint64, taskID uint64) (*models.Task, error) {
	taskM, err := u.taskRepo.FindByID(taskID)
	if err != nil {
		err := errors.ErrNotFound
		err.Message = "Task not found"
		return nil, err
	}

	if taskM.UserID != userID {
		err := errors.ErrUnauthorized
		err.Message = "This task is not authorized for this user"
		return nil, err
	}

	return taskM, nil
}
