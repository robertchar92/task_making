package module

import (
	"go.uber.org/fx"

	taskHTTP "task_making/service/task/delivery/http"
	taskRepo "task_making/service/task/repository/postgres"
	taskUsecase "task_making/service/task/usecase"
)

var Module = fx.Options(
	fx.Provide(
		taskHTTP.New,
		taskUsecase.New,
		taskRepo.New,
	),
)
