package task

import (
	"task_making/models"
	"task_making/utils/request_util"

	"gorm.io/gorm"
)

type Repository interface {
	FindAll(config request_util.PaginationConfig) ([]models.Task, error)
	Count(config request_util.PaginationConfig) (int64, error)
	FindByID(taskID uint64) (*models.Task, error)
	FindByPosition(userID uint64, position int) (*models.Task, error)
	Insert(task *models.Task, tx *gorm.DB) error
	Update(task *models.Task, tx *gorm.DB) error
	Delete(task *models.Task, tx *gorm.DB) error
	UpdateAllPositionAddByOne(userID uint64, tx *gorm.DB) error
	UpdatePositionAddByOne(userID uint64, startPosition int, endPosition int, tx *gorm.DB) error
	UpdatePositionSubByOne(userID uint64, startPosition int, endPosition int, tx *gorm.DB) error
	UpdatePositionWhenDelete(userID uint64, startPosition int, tx *gorm.DB) error
}
