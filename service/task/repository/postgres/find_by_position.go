package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"

	"gorm.io/gorm"
)

func (r *Repository) FindByPosition(userID uint64, position int) (*models.Task, error) {
	model := models.Task{}

	err := r.db.
		Where("user_id = ? AND position = ?", userID, position).
		First(&model).Error

	if err == gorm.ErrRecordNotFound {
		return nil, errors.ErrNotFound
	}

	if err != nil {
		log.Println("error-find-task-by-position:", err)
		return nil, errors.CustomWrap(err)
	}

	return &model, nil
}
