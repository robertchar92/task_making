package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"

	"gorm.io/gorm"
)

func (r *Repository) UpdateAllPositionAddByOne(userID uint64, tx *gorm.DB) error {
	var db = r.db
	if tx != nil {
		db = tx
	}
	err := db.Model(models.Task{}).Where("user_id = ?", userID).UpdateColumn("position", gorm.Expr("position + ?", 1)).Error
	if err != nil {
		log.Println("error-update-all-position-task:", err)
		return errors.CustomWrap(err)
	}
	return nil
}
