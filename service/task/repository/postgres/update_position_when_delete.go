package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"

	"gorm.io/gorm"
)

func (r *Repository) UpdatePositionWhenDelete(userID uint64, startPosition int, tx *gorm.DB) error {
	var db = r.db
	if tx != nil {
		db = tx
	}
	err := db.Model(models.Task{}).Where("user_id = ? AND position > ?", userID, startPosition).UpdateColumn("position", gorm.Expr("position - ?", 1)).Error
	if err != nil {
		log.Println("error-update-position-sub-task:", err)
		return errors.CustomWrap(err)
	}
	return nil
}
