package postgres

import (
	"log"

	"task_making/models"
	"task_making/utils/errors"
	"task_making/utils/request_util"
)

func (r *Repository) Count(config request_util.PaginationConfig) (int64, error) {
	var count int64

	err := r.db.
		Model(&models.Task{}).
		Scopes(config.Scopes()...).
		Count(&count).Error
	if err != nil {
		log.Println("error-count-task:", err)
		return 0, errors.CustomWrap(err)
	}

	return count, nil
}
