package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"

	"gorm.io/gorm"
)

func (r *Repository) Delete(task *models.Task, tx *gorm.DB) error {
	var db = r.db
	if tx != nil {
		db = tx
	}

	err := db.Delete(task).Error
	if err != nil {
		log.Println("error-delete-task:", err)
		return errors.CustomWrap(err)
	}

	return nil
}
