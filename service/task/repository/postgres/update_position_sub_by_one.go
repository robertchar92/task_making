package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"

	"gorm.io/gorm"
)

func (r *Repository) UpdatePositionSubByOne(userID uint64, startPosition int, endPosition int, tx *gorm.DB) error {
	var db = r.db
	if tx != nil {
		db = tx
	}
	err := db.Model(models.Task{}).Where("user_id = ? AND position >= ? AND position <= ?", userID, endPosition, startPosition).UpdateColumn("position", gorm.Expr("position - ?", 1)).Error
	if err != nil {
		log.Println("error-update-position-sub-task:", err)
		return errors.CustomWrap(err)
	}
	return nil
}
