package postgres

import (
	"log"

	"gorm.io/gorm"

	"task_making/models"
	"task_making/utils/errors"
)

func (r *Repository) FindByID(taskID uint64) (*models.Task, error) {
	model := models.Task{}

	err := r.db.Where("id = ?", taskID).First(&model).Error
	if err == gorm.ErrRecordNotFound {
		return nil, errors.ErrNotFound
	}

	if err != nil {
		log.Println("error-find-task-by-id:", err)
		return nil, errors.CustomWrap(err)
	}

	return &model, nil
}
