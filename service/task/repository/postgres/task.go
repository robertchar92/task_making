package postgres

import (
	"gorm.io/gorm"

	"task_making/service/task"
)

type Repository struct {
	db *gorm.DB
}

func New(db *gorm.DB) task.Repository {
	return &Repository{
		db: db,
	}
}
