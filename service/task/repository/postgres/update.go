package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"

	"gorm.io/gorm"
)

func (r *Repository) Update(task *models.Task, tx *gorm.DB) error {
	var db = r.db
	if tx != nil {
		db = tx
	}
	err := db.Save(task).Error
	if err != nil {
		log.Println("error-update-task:", err)
		return errors.ErrUnprocessableEntity
	}

	return nil
}
