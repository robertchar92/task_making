package postgres

import (
	"log"
	"task_making/models"
	"task_making/utils/errors"
	"task_making/utils/request_util"
)

func (r *Repository) FindAll(config request_util.PaginationConfig) ([]models.Task, error) {
	results := make([]models.Task, 0)

	err := r.db.
		Scopes(config.MetaScopes()...).
		Scopes(config.Scopes()...).
		Find(&results).Error
	if err != nil {
		log.Println("error-find-task:", err)
		return nil, errors.CustomWrap(err)
	}

	return results, nil
}
