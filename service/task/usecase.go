package task

import (
	"task_making/models"
	"task_making/service/task/delivery/http/request"
	"task_making/utils/request_util"
	response_util "task_making/utils/response_utils"
)

type Usecase interface {
	Index(userID uint64, paginationConfig request_util.PaginationConfig) ([]models.Task, response_util.PaginationMeta, error)
	Show(userID uint64, taskID uint64) (*models.Task, error)
	Create(userID uint64, request request.TaskCreateRequest) (*models.Task, error)
	Update(userID uint64, taskID uint64, request request.TaskUpdateRequest) (*models.Task, error)
	Delete(userID uint64, taskID uint64) error
}
