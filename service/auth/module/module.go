package module

import (
	"go.uber.org/fx"

	authHTTP "task_making/service/auth/delivery/http"
	authUsecase "task_making/service/auth/usecase"
)

var Module = fx.Options(
	fx.Provide(
		authHTTP.New,
		authUsecase.New,
	),
)
