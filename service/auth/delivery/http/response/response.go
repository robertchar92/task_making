package response

import "task_making/models"

type AuthResponse struct {
	Token string      `json:"token" groups:"user"`
	User  models.User `json:"user" groups:"user"`
}
