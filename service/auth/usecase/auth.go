package usecase

import (
	"log"
	"os"
	"task_making/models"
	"task_making/service/auth"
	"task_making/service/user"
	"task_making/utils/errors"
	"time"

	"task_making/lib/database_transaction"

	"github.com/golang-jwt/jwt/v4"
)

type Usecase struct {
	userRepo           user.Repository
	transactionManager database_transaction.Client
}

type authClaims struct {
	jwt.StandardClaims
	Username string `json:"username"`
	Role     string `json:"role"`
	Id       uint64 `json:"id"`
	IssuedAt int64  `json:"orig_iat,omitempty"`
}

func New(
	userRepo user.Repository,
	transactionManager database_transaction.Client,
) auth.Usecase {
	return &Usecase{
		userRepo:           userRepo,
		transactionManager: transactionManager,
	}
}

func generateUserTokenString(userM *models.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, authClaims{
		Id:       userM.ID,
		Username: userM.Username,
		Role:     models.RoleNameUser,
		IssuedAt: time.Now().Unix(),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		},
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		log.Println("error-encoding-token", err)
		return "", errors.ErrUnprocessableEntity
	}

	return tokenString, nil
}
