package auth

import (
	"task_making/service/auth/delivery/http/request"
	"task_making/service/auth/delivery/http/response"
)

type Usecase interface {
	SignUp(request request.SignUpRequest) error
	SignIn(request request.SignInRequest) (response.AuthResponse, error)
}
