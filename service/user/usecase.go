package user

import (
	"task_making/models"
	"task_making/service/user/delivery/http/request"
)

type Usecase interface {
	Show(userID uint64, unscoped ...bool) (*models.User, error)
	Update(userID uint64, request request.UserUpdateRequest) (*models.User, error)
}
