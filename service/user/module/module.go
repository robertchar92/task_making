package module

import (
	"go.uber.org/fx"

	userHTTP "task_making/service/user/delivery/http"
	userRepo "task_making/service/user/repository/postgres"
	userUsecase "task_making/service/user/usecase"
)

var Module = fx.Options(
	fx.Provide(
		userHTTP.New,
		userUsecase.New,
		userRepo.New,
	),
)
