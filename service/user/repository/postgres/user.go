package postgres

import (
	"gorm.io/gorm"

	"task_making/service/user"
)

type Repository struct {
	db *gorm.DB
}

func New(db *gorm.DB) user.Repository {
	return &Repository{
		db: db,
	}
}
