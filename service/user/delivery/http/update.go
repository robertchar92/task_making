package http

import (
	"fmt"
	"net/http"
	"strconv"
	"task_making/service/user/delivery/http/request"
	"task_making/utils/errors"
	"task_making/utils/role"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

func (h *Handler) Update(c *gin.Context) {
	claims := jwt.ExtractClaims(c)

	userID, err := strconv.ParseUint(fmt.Sprint(claims["id"]), 10, 64)
	if err != nil {
		_ = c.Error(errors.ErrUnprocessableEntity).SetType(gin.ErrorTypePublic)
	}

	var req request.UserUpdateRequest

	// validate request
	if err := c.ShouldBind(&req); err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypeBind)
		return
	}

	res, err := h.userUsecase.Update(userID, req)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	data, err := role.GetDataJSONByRole(res, role.User)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	c.JSON(http.StatusOK, data)
}
