package http

import (
	"fmt"
	"net/http"
	"strconv"
	"task_making/utils/errors"
	"task_making/utils/role"

	jwt "github.com/appleboy/gin-jwt/v2"

	"github.com/gin-gonic/gin"
)

func (h *Handler) Show(c *gin.Context) {
	claims := jwt.ExtractClaims(c)

	userID, err := strconv.ParseUint(fmt.Sprint(claims["id"]), 10, 64)
	if err != nil {
		_ = c.Error(errors.ErrUnprocessableEntity).SetType(gin.ErrorTypePublic)
	}

	res, err := h.userUsecase.Show(userID, true)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	data, err := role.GetDataJSONByRole(res, role.User)
	if err != nil {
		_ = c.Error(err).SetType(gin.ErrorTypePublic)
		return
	}

	c.JSON(http.StatusOK, data)
}
