package usecase

import "task_making/service/user"

type Usecase struct {
	userRepo user.Repository
}

func New(
	userRepo user.Repository,
) user.Usecase {
	return &Usecase{
		userRepo: userRepo,
	}
}
