package usecase

import "task_making/models"

func (u *Usecase) Show(userID uint64, unscoped ...bool) (*models.User, error) {
	userM, err := u.userRepo.FindByID(userID, unscoped...)
	if err != nil {
		return nil, err
	}

	return userM, nil
}
