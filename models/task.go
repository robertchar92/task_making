package models

import (
	"time"

	"gorm.io/gorm"
)

type Task struct {
	ID          uint64          `json:"id" groups:"user,admin"`
	Name        string          `json:"name" groups:"user,admin"`
	Description string          `json:"description" groups:"user,admin"`
	UserID      uint64          `json:"user_id" groups:"user,admin"`
	Position    int             `json:"position" groups:"user,admin"`
	IsComplete  bool            `json:"is_complete" groups:"user,admin"`
	CreatedAt   time.Time       `json:"created_at" groups:"user,admin"`
	UpdatedAt   time.Time       `json:"-"`
	DeletedAt   *gorm.DeletedAt `json:"-"`
}
