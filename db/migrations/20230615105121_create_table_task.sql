-- +goose Up
-- +goose StatementBegin
CREATE TABLE tasks (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    description TEXT NOT NULL,
    user_id BIGSERIAL NOT NULL,
    position INTEGER NOT NULL,
    is_complete BOOLEAN NOT NULL DEFAULT false,
    created_at TIMESTAMP NOT NULL,
    updated_at TIMESTAMP NOT NULL,
    deleted_at TIMESTAMP
);

CREATE INDEX task_user_id_idx ON tasks (user_id);
CREATE INDEX tasks_position_idx ON tasks (position);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE tasks;
-- +goose StatementEnd
